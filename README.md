# `elvish.kak`
[Kakoune](https://kakoune.org) language file for editing [Elvish](https://elvish.io) scripts.

Curly brackets indentation only works when the opening bracket is the last character in the line, and the closing is the first in the line:
```elvish
# This works.

fn example {
  kak
  if $true {
    ls
    cat text.txt
    cd ~
  }
}
```

```elvish
# This don't work, because the opening bracket isn't the last character.
# However, it is less readable than the above formatting.

fn example { kak
if $true { ls
cat text.txt
cd ~ }
}
```
